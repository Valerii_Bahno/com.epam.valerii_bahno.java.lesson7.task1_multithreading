import java.util.Scanner;

public class Multithreads {

    public static void main(String[] args) throws InterruptedException {

        Scanner in = new Scanner(System.in);
        System.out.println("Enter from:");
        int from = in.nextInt();
        System.out.println("Enter to:");
        int to = in.nextInt();
        System.out.println("Enter count of threads:");
        int countThreads = in.nextInt();

        for (int i = 0; i < countThreads; i++) {
            Thread thread = new Thread(new MyThread(from, to, countThreads));
            thread.start();
            thread.join();
        }
    }
}